import random
import math
import os
import copy

os.system('cls')

BLACK = '\033[30m'
RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
BLUE = '\033[34m'
MAGENTA = '\033[35m'
CYAN = '\033[36m'
WHITE = '\033[37m'
RESET = '\033[39m'

#Generador de laberinto
def lab_gen (n):
    
    #Creación en blanco del mapa
    lab1 = []
    for a in range(n):
        lab1.append([])
        for b in range(n):
            lab1[a].append(' ')


    lab1[0][0] = 1
    lab1[n-1][n-1]= 0
    x = 0   #Variable de movimiento Horizontal
    y = 0   #Variable de movimiento Vertical


    #Generación del camino
    while (lab1[n-1][n-1] == 0):
        gen = 0     #Variable de generación aleatoria

        #Standart Derecha y Abajo 
        if ((x != n-1) and (y != n-1)):
            gen = random.randint(1,4)
            if (gen == 1) or (gen == 2):
                x = x + 1
                lab1[y][x] = 1

            
            if (gen == 3) or (gen == 4):
                y = y + 1
                lab1[y][x] = 1

        #Movimiento condicional borde inferior SOLO HACIA ABAJO
        if (x == n-1) and (y != n-1):
            y = y + 1
            lab1[y][x] = 1

        #Movimiento condicional borde derecho SOLO HACIA ARRIBA
        if (y == n-1) and (x != n-1):
            x = x + 1
            lab1[y][x] = 1

    return lab1


#Generacion de radicales
def rad_gen (lab1, n, x, y):
    gen = 0     #Variable aleatoria para definir el recorrido del radical
    stop = 0    #Variable que determina el final de la generacion


    while (stop != 1):
        gen = random.randint(0,4)

        #Condicion de las esquinas
        if (x == 0) and (y == 0):
            stop = 1

        if (x == 0) and (y == n-1):
            stop = 1

        if (x == n-1) and (y == 0):
            stop = 1

        if (x == n-1) and (y == n-1):
            stop = 1

        #Creacion Derecha
        if (gen == 0) and (x != n-1):
            if (lab1[y][x+1] == " "):
                lab1[y][x+1] = 0
                x = x+1

        #Creacion Abajo
        if (gen == 1) and (y != n-1):
            if (lab1[y+1][x] == " "):
                lab1[y +1][x] = 0
                y = y+1

        #Creacion Izquierda
        if (gen == 2) and (x != 0):
            if (lab1[y][x-1] == " "):
                lab[y][x-1] = 0
                x = x-1

        #Creacion Arriba
        if (gen == 3) and (y != 0):
            if (lab1[y-1][x] == " "):
                lab1[y-1][x] = 0
                y = y-1

        #Stop
        if (gen == 4):
            stop = 1

    return lab1


#Conversor de radicales a caminos recorribles
def convert(lab1, n):
    for a in range (n):
        for b in range (n):
            if lab[a][b] == 0:
                lab [a][b] = 1

    return lab1


#FUNCIÓN DE MOVIMIENTO

def movimiento(lab1, n):
    os.system('cls')
    movs = 0
    y = 0
    x = 0
    finish = 0
    ending = 0
    lab1[y][x] = 2 
    lab1[n-1][n-1] = 3


    while finish != 1:
        os.system('cls')
        print('')

        #IMPRESIÖN DEL LABERINTO
        for c in range (n):
            for d in range (n):
                if (lab1[c][d] == 1):
                    print(BLUE+'■', end=' ')
                if (lab1[c][d] == 2):
                    print(RED+'■', end=' ')
                if (lab1[c][d] == 3):
                    print(GREEN+'■', end=' ')
                if (lab1[c][d] == " "):
                    print(RED+' ', end=' ')
            print('')


        #INGRESO DE COMANDO O TECLA PARA MOVERSE

        print(RESET+'\nPara interactuar debe escribir la acción y presionar [ENTER] \n \nControles:\n')
        print('Movimiento:     Viajar al inicio:    Salir:     Resetear el mapa')
        print(CYAN+'    [w]')
        print('[a] [s] [d]         [INICIO]         [EXIT]          [RESET]\n')
        print(RESET+'')
        tecla = input('Acción : ')
        

        #MOVIMIENTO HACIA ARRIBA
        if tecla.upper() == 'W':
            if y == 0:
                continue
            elif lab1[y-1][x] == 1:
                lab1[y-1][x] = 2
                lab1[y][x] = 1
                y = y - 1
                movs = movs +1
            elif lab1[y-1][x] == 3:
                lab1[y-1][x] = 2
                lab1[y][x] = 1
                y = y - 1
                ending = 1
                movs = movs +1
                return ending,movs
            else:
                continue

        #MOVIMIENTO HACIA ABAJO
        elif tecla.upper() == 'S':
            if y == n-1:
                continue
            elif lab1[y+1][x] == 1:
                lab1[y+1][x] = 2
                lab1[y][x] = 1
                y = y + 1
                movs = movs +1
            elif lab1[y+1][x] == 3:
                lab1[y+1][x] = 2
                lab1[y][x] = 1
                y = y + 1
                ending = 1
                movs = movs +1
                return ending,movs
            else:
                continue


        #MOVIMIENTO HACIA LA IZQUIERDA
        elif tecla.upper() == 'A':
            if x == 0:
                continue
            elif lab1[y][x-1] == 1:
                lab1[y][x-1] = 2
                lab1[y][x] = 1
                x = x - 1
                movs = movs +1
            elif lab1[y][x-1] == 3:
                lab[y][x-1] = 2
                lab1[y][x] = 1
                x = x - 1
                ending = 1
                movs = movs +1
                return ending,movs
            else:
                continue


        #MOVIMIENTO HACIA LA DERECHA
        elif tecla.upper() == 'D':
            if x == n-1:
                continue
            elif lab1[y][x+1] == 1:
                lab1[y][x+1] = 2
                lab1[y][x] = 1
                x = x + 1
                movs = movs +1
            elif lab1[y][x+1] == 3:
                lab1[y][x+1] = 2
                lab1[y][x] = 1
                x = x + 1
                ending = 1
                movs = movs +1
                return ending,movs
            else:
                continue

        #INICIAR DESDE LA POSICION INICIAL
        elif tecla.upper() == 'INICIO':
            lab1[y][x] = 1
            lab1[0][0] = 2
            lab1[n-1][n-1] = 3
            y = 0
            x = 0
            movs = movs +1

        #RESET GENERACION DEL LABERINTO
        elif tecla.upper() == 'RESET':
            ending = 2
            return ending,movs


        #OPCION PARA SALIR
        elif tecla.upper() == 'EXIT':
            ending = 3
            return ending,movs


        #CUALQUIER OTRO CASO
        else:
            continue






#BLOQUE PRINCIPAL

#TAMAÑO DEL LABERINTO
try:
    print(CYAN+'██╗░░░░░░█████╗░██████╗░███████╗██████╗░██╗███╗░░██╗████████╗░█████╗░')
    print('██║░░░░░██╔══██╗██╔══██╗██╔════╝██╔══██╗██║████╗░██║╚══██╔══╝██╔══██╗')
    print('██║░░░░░███████║██████╦╝█████╗░░██████╔╝██║██╔██╗██║░░░██║░░░██║░░██║')
    print('██║░░░░░██╔══██║██╔══██╗██╔══╝░░██╔══██╗██║██║╚████║░░░██║░░░██║░░██║')
    print('███████╗██║░░██║██████╦╝███████╗██║░░██║██║██║░╚███║░░░██║░░░╚█████╔╝')
    print('╚══════╝╚═╝░░╚═╝╚═════╝░╚══════╝╚═╝░░╚═╝╚═╝╚═╝░░╚══╝░░░╚═╝░░░░╚════╝░')
    print(YELLOW+'                                                         Daniel Tobar')
    print('                                                       Cristobal Díaz')
    print('                                                       Angel Guerrero\n')
    print(RESET+'')

    n = int(input('Indique el tamaño del laberinto: '))
    print('')


    rads = 0 #CANTIDAD DE RADICALES
    lab = lab_gen(n)

    for a in range (n):
        for b in range (n):
            rads = random.randint(0,2)
            
            if rads == 0:
                for i in range (2):
                    if lab[a][b] == 1:
                        rad_gen(lab, n, b, a)
            
            if rads == 1:
                if lab [a][b] == 1:
                    rad_gen(lab, n, b, a)
            
            if rads == 2:
                pass
    map = convert(lab, n)

    #METODO DE RESET

    opcion = 2
    while opcion == 2:
        ending,movs = movimiento(map, n)

        if ending == 1:
            os.system('cls')
            print(GREEN+"█▀▀ ▄▀█ █▄░█ ▄▀█ █▀ ▀█▀ █▀▀")
            print('█▄█ █▀█ █░▀█ █▀█ ▄█ ░█░ ██▄\n')
            print(RESET+'')
            print(f"Total de movimientos: {movs}\n")
            print("Esciba EXIT para salir o RESET para volver a empezar")
            final = input('Opción: ')
            if final.upper() == 'RESET':
                opcion = 2
            elif final.upper() == 'EXIT':
                opcion = 3
            else:
                pass
        elif ending == 2:
            pass
        elif ending == 3:
            break
        else:
            pass


        if opcion == 2:
            lab = lab_gen(n)

            for a in range (n):
                for b in range (n):
                    rads = random.randint(0,2)

                    if rads == 0:
                        for i in range (2):
                            if lab[a][b] == 1:
                                rad_gen(lab, n, b, a)

                    if rads == 1:
                        if lab [a][b] == 1:
                                rad_gen(lab, n, b, a)

                    if rads == 2:
                        pass
            map = convert(lab, n)
        else:
            break

    print("Juego terminado")
    b = 0
except ValueError: 
    print(RED+"ATENCION: ingrese un valor entero")