==== Laberinto ====

Este proyecto consta en la creación de un laberinto recorrible generado aleatorimente con
múltiples caminos, el cual el usuario comienza en la esquina superior izquierda y su meta
es la esquina inferior derecha.


==== Pre-Requisitos ====
>Python 3


Para ejecutar este proyecto se debe abrir con python, una vez abierto entregará el
siguiente mensaje:

>Indique el tamaño del laberinto: 

En el cual el usuario debe ingresar el tamaño cuadricular del laberinto, una vez
ingresado el número comenzará la creación del laberinto y la generación del
camino y sus caminos alternos. Posteriormente a la creación del mapa se mostrará
en pantalla y pedirá al usuario una tecla de movimiento (w,a,s,d,W,A,S,D) o una 
función de movimiento (EXIT, INICIO, RESET)para mover al "Jugador" que está 
representado por una X. Un ejeplo de una de las generaciones:


***INICIO DE EJEMPLO***

■ ■ ■ ■ ■ ■
■ ■ ■ ■ ■ ■
■     ■ ■ ■   ■ ■ ■
■ ■   ■ ■ ■ ■ ■ ■ ■
        ■ ■ ■ ■ ■ ■
                ■ ■
              ■ ■ ■
              ■ ■ ■
                ■ ■
              ■ ■ ■

Para interactuar debe escribir la acción y presionar [ENTER]

Controles:

Movimiento:     Viajar al inicio:    Salir:     Resetear el mapa
    [w]
[a] [s] [d]         [INICIO]         [EXIT]          [RESET]


Acción :


***FIN DE EJEMPLO***


Cuando la X llegue a la esquina inferior derecha se desplegará un mensaje indicando el
número de movimientos y dos opciones, salir y reiniciar.


***INICIO DE EJEMPLO***


█▀▀ ▄▀█ █▄░█ ▄▀█ █▀ ▀█▀ █▀▀
█▄█ █▀█ █░▀█ █▀█ ▄█ ░█░ ██▄


Total de movimientos: 18

Esciba EXIT para salir o RESET para volver a empezar
Opción:


***FIN DE EJEMPLO***


==== CONSTRUIDO CON ====
Visual Studio Code - Editor de lineas de texto 



==== VERSIONES ====
Este proyecto cuenta con 2 versiones, la versión de Windows y la verión de Linux, como su nombre
indican despues del guión:

> laberinto-linux.py		<== Versión de Linux
> laberinto-windows.py		<== Versión de Windows


==== DESARROLLADO POR: ====
Angel Guerrero
Daniel Tobar
Cristobal Díaz